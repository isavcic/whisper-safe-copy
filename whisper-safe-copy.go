package main

import (
	"fmt"
	"io"
	"os"
	"runtime"
	"strings"
	"syscall"
	"time"

	"github.com/fatih/structs"
	"github.com/karrick/godirwalk"
	"github.com/korovkin/limiter"
	flock "github.com/theckman/go-flock"
)

func main() {

	limit := limiter.NewConcurrencyLimiter(100)

	var srcDir, dstDir string

	if len(os.Args) == 3 {
		srcDir = os.Args[1]
		dstDir = os.Args[2]
	} else {
		fmt.Println("Need two parameters: src dir and dst dir")
		os.Exit(1)
	}

	err := godirwalk.Walk(srcDir, &godirwalk.Options{
		Unsorted: true,
		Callback: func(osPathname string, de *godirwalk.Dirent) error {
			relDst := strings.Replace(osPathname, srcDir, "", -1)
			if de.ModeType().IsDir() {
				if osPathname != srcDir {
					// fmt.Println("mkdir", dstDir+relDst)
					os.MkdirAll(dstDir+relDst, 0755)
					// Can't copy metadata here, because the mtime will get reset by comying the file into the dir...
					// err := syncMetadata(osPathname, dstDir+relDst)
					// if err != nil {
					// 	fmt.Println(err)
					// }
				}
			} else {
				// fmt.Println("   cp", osPathname, "to", dstDir+relDst)
				limit.Execute(func() {
					copyFile(osPathname, dstDir+relDst)
				})
			}
			return nil
		},
		ErrorCallback: func(osPathname string, err error) godirwalk.ErrorAction {
			// Your program may want to log the error somehow.
			fmt.Fprintf(os.Stderr, "ERROR: %s\n", err)

			// For the purposes of this example, a simple SkipNode will suffice,
			// although in reality perhaps additional logic might be called for.
			return godirwalk.SkipNode
		},
		PostChildrenCallback: func(osPathname string, de *godirwalk.Dirent) error {
			relDst := strings.Replace(osPathname, srcDir, "", -1)
			err := syncMetadata(osPathname, dstDir+relDst)
			if err != nil {
				fmt.Println(err)
			}
			return nil
		},
	})
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(1)
	}
	limit.Wait()
}

func copyFile(src, dst string) (err error) {
	in, err := os.Open(src)
	if err != nil {
		return
	}
	defer in.Close()

	out, err := os.Create(dst)
	if err != nil {
		return
	}

	defer func() {
		if e := out.Close(); e != nil {
			err = e
		}
	}()

	srcLock := flock.NewFlock(src)

	var locked bool

	for !locked {
		locked, err = srcLock.TryLock()

		if !locked {
			fmt.Println("No lock on", src, "... Retrying")
			time.Sleep(100 * time.Millisecond)
		}
	}

	_, err = io.Copy(out, in)
	if err != nil {
		return
	}

	srcLock.Unlock()

	// err = out.Sync()
	// if err != nil {
	// 	return
	// }

	err = syncMetadata(src, dst)
	if err != nil {
		return
	}

	// })

	return
}

func syncMetadata(src, dst string) (err error) {
	// fmt.Printf("Copying metadata from %s to %s\n", src, dst)

	mode, uid, gid, atime, mtime, _, err := getMetadata(src)
	if err != nil {
		return
	}

	err = os.Chmod(dst, mode)
	if err != nil {
		return
	}

	err = os.Chtimes(dst, timespecToTime(atime), timespecToTime(mtime))
	if err != nil {
		return
	}

	err = os.Chown(dst, uid, gid)
	if err != nil {
		return
	}

	return
}

func getMetadata(name string) (mode os.FileMode, uid, gid int, atime, mtime, ctime syscall.Timespec, err error) {
	fi, err := os.Stat(name)
	if err != nil {
		return
	}

	mode = fi.Mode()
	stat := fi.Sys().(*syscall.Stat_t)
	uid = int(stat.Uid)
	gid = int(stat.Gid)

	s := structs.New(stat)

	if runtime.GOOS == "darwin" {
		mtime = s.Field("Mtimespec").Value().(syscall.Timespec)
		atime = s.Field("Atimespec").Value().(syscall.Timespec)
		ctime = s.Field("Ctimespec").Value().(syscall.Timespec)
		return
	} else if runtime.GOOS == "linux" {
		mtime = s.Field("Mtim").Value().(syscall.Timespec)
		atime = s.Field("Atim").Value().(syscall.Timespec)
		ctime = s.Field("Ctim").Value().(syscall.Timespec)
		return
	}
	return
}

func timespecToTime(ts syscall.Timespec) time.Time {
	return time.Unix(int64(ts.Sec), int64(ts.Nsec))
}
